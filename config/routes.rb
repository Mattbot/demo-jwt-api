Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'admin/dashboard#index'

  namespace :api do
    namespace :v1 do
      post 'api_user_token' => 'api_user_token#create'
      resources :api_users, only: %i[show index] do
      end
    end
  end
end
