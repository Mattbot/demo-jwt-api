# Demo JWT API

## Overview
This is a Rails 5.2 app which manages API users and issues them an [JWT][jwt]
token for use with other micro-service APIs.  This app is docker ready and meant
to run in a clustered docker deployment.  The JWT HMAC secret is loaded into
the app via an ENV variable which is to be stored in the docker cluster's
discovery service, i.e. Etcd, etc.

## API Endpoints
There are two API endpoints:

### Authentication
To retrive a user's JWT token:
```
POST /api/v1/api_user_token
```
JSON-encoded body:
```json
{ "auth": {
    "email": "user@example.com",
    "password": "secret"
  }
}

```
The decoded payload portion contains:
```json
{
  "exp": "{expiry_time}",
  "id": "{user_id}",
  "email": "{user_email}"
}
```

### User Information
This API call isn't really neccessary as the token contains the id and email:
```
Add to header: Authorization: Bearer {JWT_returned_from_/api/v1/api_user_token}
GET /api/v1/api_user/{user_id}
```

```json
{
    "data": {
        "id": "{user_id}",
        "type": "api-users",
        "attributes": {
            "email": "{email}"
        }
    }
}
```


[jwt]: https://jwt.io/introduction/
