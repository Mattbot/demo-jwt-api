class Api::V1::ApiUserSerializer < Api::V1::BaseSerializer
  attributes :id, :email, :username
end
