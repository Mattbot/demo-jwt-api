class ApiUser < ApplicationRecord
  has_secure_password

  def self.from_token_payload(payload)
    find(payload['sub'])
  end

  def to_token_payload
    { user_id: id, email: email, username: username }
  end
end
