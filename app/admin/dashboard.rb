ActiveAdmin.register_page 'Dashboard' do
  menu priority: 1, label: proc { I18n.t('active_admin.dashboard') }

  content title: proc { I18n.t('active_admin.dashboard') } do
    columns do
      column do
        panel 'Recent Api Users' do
          ul do
            ApiUser.last(5).map do |api_user|
              li link_to(api_user.email, admin_api_user_path(api_user))
            end
          end
        end
      end
      column do
        panel 'Recent Admin Users' do
          ul do
            AdminUser.last(5).map do |admin_user|
              li link_to(admin_user.email, admin_admin_user_path(admin_user))
            end
          end
        end
      end
    end
  end
end
