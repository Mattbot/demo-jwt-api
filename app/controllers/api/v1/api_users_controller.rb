class Api::V1::ApiUsersController < Api::V1::BaseController
  # before_action :authenticate_api_user

  def show
    @api_user = ApiUser.find(params[:id])
    render json: @api_user
  end

  def index
    @api_users = ApiUser.all
    render json: @api_users
  end
end
