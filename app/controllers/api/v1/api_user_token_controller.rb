class Api::V1::ApiUserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token
end
