require 'test_helper'

class ApiUserControllerTest < ActionDispatch::IntegrationTest
  def authenticated_header
    token = Knock::AuthToken.new(payload: { sub: api_users(:api_user_one).id }).token
    { 'Authorization': "Bearer #{token}" }
  end

  test 'responds successfully with token' do
    get('/api/v1/api_users/1', headers: authenticated_header)
    assert_response :success
  end

  test 'responds unsuccessfully with wrong token' do
    get('/api/v1/api_users/2', headers: authenticated_header)
    assert_response :unauthorized
  end

  test 'responds unsuccessfully with invalid token' do
    get('/api/v1/api_users/2', headers: { 'Authorization': "Bearer horsepoo" })
    assert_response :unauthorized
  end

  test 'responds unsuccessfully without token' do
    get('/api/v1/api_users/1')
    assert_response :unauthorized
  end
end
