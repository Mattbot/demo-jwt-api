#!/bin/bash

set -euo pipefail

git diff --quiet || [[ ${BUILD_TEST:-} ]] || (echo 'Commit your changes first!'; exit 1)

# attempt this now for fail-fast, even though it could expire by the time the image is built, so we'll not use the output this time:
aws ecr get-login --no-include-email --region us-east-1 2>&1 > /dev/null

# if you run this with a DOCKER_MACHINE_NAME, you don't have to manually run `docker-machine env` nor `unset DOCKER_HOST ...`
[[ ${DOCKER_MACHINE_NAME:-} ]] && eval $(docker-machine env $DOCKER_MACHINE_NAME)
[[ $? == 0 ]] || exit 1

project_name='demo-authentication-service'
git_revision="$(git rev-parse HEAD)"
docker_repo="${AWS_ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/${project_name}"
docker_image="${docker_repo}:${git_revision}"

if [[ ${BUILD_TEST:-} ]]; then
    docker-compose build app
else
    docker-compose build --pull app
fi

cid="$(docker create "${COMPOSE_PROJECT_NAME}_app")"

docker commit \
       -c "LABEL net.mattbot.application.git-revision=${git_revision}" \
       -c "LABEL net.mattbot.application.name=${project_name}" \
       -c "ENV GIT_REVISION ${git_revision}" \
       -c 'HEALTHCHECK --interval=5s --timeout=30s --retries=3 CMD ["health_check"]' \
       "$cid" \
       "$docker_image"

echo
echo "docker image = $docker_image"
echo

login_cmd="$(aws ecr get-login --no-include-email --region us-east-1)"
[[ $? == 0 ]] || exit 1
eval $login_cmd

[[ ${BUILD_TEST:-} ]] && exit

docker push "${docker_image}"
