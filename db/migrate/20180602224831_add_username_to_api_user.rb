class AddUsernameToApiUser < ActiveRecord::Migration[5.2]
  def change
    add_column :api_users, :username, :string
  end
end
