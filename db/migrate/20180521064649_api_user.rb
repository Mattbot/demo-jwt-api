class ApiUser < ActiveRecord::Migration[5.2]
  def change
    create_table :api_users do |t|
      t.string :email,              null: false, default: ""
      t.string :password_digest,    null: false, default: ""
      t.timestamps null: false
    end

    add_index :api_users, :email,                unique: true
  end
end
